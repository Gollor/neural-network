package main;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main 
{
	static class Sample
	{
		double Image[] = new double[784];
		int Label;
		Sample (double _Image[], int _Label)
		{
			Image = _Image;
			Label = _Label;
		}
	}
	static Sample TrainSamples[] = new Sample[60000];
	static Sample TestSamples[] = new Sample[10000];
	
	public static void main(String[] args) throws IOException
	{
		
		Network Net = new Network(784);
		Net.AddLayer(50);
		Net.AddLayer(10);

		String TrainImagesFile = "train-images.idx3-ubyte";
		String TrainLabelsFile = "train-labels.idx1-ubyte";
		String TestImagesFile = "t10k-images.idx3-ubyte";
		String TestLabelsFile = "t10k-labels.idx1-ubyte";
		
		byte[] TrainImages = new byte[50000000];
		byte[] TrainLabels = new byte[80000];
		byte[] TestImages = new byte[10000000];
		byte[] TestLabels = new byte[20000];
		
		BufferedInputStream TrainImagesInputStream = new BufferedInputStream(new FileInputStream(TrainImagesFile));
		TrainImagesInputStream.read(TrainImages);
		TrainImagesInputStream.close();

		BufferedInputStream TrainLabelsInputStream = new BufferedInputStream(new FileInputStream(TrainLabelsFile));
		TrainLabelsInputStream.read(TrainLabels);
		TrainLabelsInputStream.close();
		
		BufferedInputStream TestImagesInputStream = new BufferedInputStream(new FileInputStream(TestImagesFile));
		TestImagesInputStream.read(TestImages);
		TestImagesInputStream.close();

		BufferedInputStream TestLabelsInputStream = new BufferedInputStream(new FileInputStream(TestLabelsFile));
		TestLabelsInputStream.read(TestLabels);
		TestLabelsInputStream.close();
		
		for (int i = 0; i < 60000; i++)
		{
			double Input[] = new double[800];
			for (int j = 0; j < 784; j++)
				Input[j] = Byte.toUnsignedInt(TrainImages[j+16+i*784])/255.0;
			int Result = Byte.toUnsignedInt(TrainLabels[8+i]);
			TrainSamples[i] = new Sample(Input,Result);
			
		}
		for (int i = 0; i < 10000; i++)
		{
			double Input[] = new double[800];
			for (int j = 0; j < 784; j++)
				Input[j] = Byte.toUnsignedInt(TestImages[j+16+i*784])/255.0;
			int Result = Byte.toUnsignedInt(TestLabels[8+i]);
			TestSamples[i] = new Sample(Input,Result);
		}
		
		for (int i = 0; i < 50; i++)
		{
			System.out.print("Epoch " + (i+1));
			List<Sample> ToTrain = new ArrayList<Sample>();
			for (int j = 0; j < 60000; j++)
				ToTrain.add(TrainSamples[j]);
			Collections.shuffle(ToTrain, new Random(System.currentTimeMillis()));
			for (int j = 0; j < 60000; j++)
			{
				Sample z = ToTrain.get(j);
				Net.Train(z.Image, z.Label);
				//System.out.print("<" + z.Label + ">");
			}
			int Win = 0;
			for (int j = 0; j < 10000; j++)
			{
				//System.out.print(Net.ProcessNoLog(TestSamples[j].Image) + "|" + TestSamples[j].Label + "!!!");
				if (TestSamples[j].Label == Net.ProcessNoLog(TestSamples[j].Image)) Win++;
			}
			System.out.println(" " + (Win/100.0));
		}
		System.out.println("Processing is finished!");
		
		//System.out.println("Total successful attempts: " + Win);
		//System.out.println("Success percentage: " + (Win/100.0));
		
		while (true) {;}
	}
}
/*
			Net.Train(Input, Result);
			if (i == 0) System.out.println("[00000/60000]");
			if (i == 9999) System.out.println("[10000/60000]");
			if (i == 19999) System.out.println("[20000/60000]");
			if (i == 29999) System.out.println("[30000/60000]");
			if (i == 39999) System.out.println("[40000/60000]");
			if (i == 49999) System.out.println("[50000/60000]");
			if (i == 59999) System.out.println("[60000/60000]");
			
			
			if (Result == Net.ProcessNoLog(Input)) Win++;
 */
