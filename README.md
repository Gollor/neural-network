Very simple neural network of perceptron type. It has sigmoid activation neurons and uses backpropagation learning method.
 
Using MNIST handwritten digit data I achieved 96.85% success rate using the 784-50-10 net with 0.15 learning rate, -0.5...0.5 linear random for weights and biases, 26 shuffling epochs.

Using committee of 5 784-80-1 nets with the same parameters as above i achieved 98.12% accuracy at 46 epoch.

MNIST handwritten digit data contains 60 000 digit images for training and 10 000 for testing. It is often used to review neural networks. 

To run this program requires all 4 MNIST data files.