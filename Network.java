package main;

import java.util.Random;

public class Network 
{
	class Layer
	{
		private int PrevResult = 0;
		private boolean IsFirst = false;
		private Layer PrevLayer;
		private Random Rand;
		private int CellAmount, PrevLayerCellAmount;
		private double Activities[], Biases[], Weights[][], Errors[];
		private double TotalError;
		private double NodeDeltas[];
		Layer(int _CellAmount, Layer _PrevLayer) //Standard Layer
		{
			Rand = new Random(System.currentTimeMillis());
			PrevLayer = _PrevLayer;
			CellAmount = _CellAmount;
			PrevLayerCellAmount = PrevLayer.GetCellAmount();
			Activities = new double[CellAmount];
			Biases = new double[CellAmount];
			Weights = new double [CellAmount][PrevLayerCellAmount];
			Errors = new double[CellAmount];
			NodeDeltas = new double[CellAmount];
			for (int i = 0; i < CellAmount; i++)
			{
				Activities[i] = 0;
				Biases[i] = Rand.nextInt(201)/200.0-0.5;
				for (int j = 0; j < PrevLayerCellAmount; j++)
					Weights[i][j] = Rand.nextInt(201)/200.0-0.5;
			}	
		}
		Layer(int _CellAmount) //First Layer
		{
			IsFirst = true;
			CellAmount = _CellAmount;
			Activities = new double[CellAmount];
			Biases = new double[CellAmount];
			Weights = new double [CellAmount][0];
			Errors = new double[CellAmount];
			NodeDeltas = new double[CellAmount];
			for (int i = 0; i < CellAmount; i++)
			{
				Activities[i] = 0;
				Biases[i] = 0;
			}	
		}
		void FeedForward()
		{
			for (int i = 0; i < CellAmount; i++)
			{
				double Sum = 0;
				for (int j = 0; j < PrevLayerCellAmount; j++)
				{
					Sum += PrevLayer.GetActivity(j)*Weights[i][j];
				}
				Sum += Biases[i];
				Activities[i] = 1.0/(1.0 + Math.exp(-Sum));//Sigmoid function
			}
		}
		void FindError(int Result)
		{
			TotalError = 0;
			for (int i = 0; i < CellAmount; i++)
			{
				if (Result == i) Errors[i] = Math.pow(0.99-Activities[i],2)/2;
				else Errors[i] = Math.pow(0.01 - Activities[i], 2)/2;
				TotalError += Errors[i];
			}
		}
		void StartBackPropagate(int Result, double LearnRate)
		{
			PrevResult = Result;
			for (int i = 0; i < CellAmount; i++)
			{
				if (Result == i) NodeDeltas[i] = (Activities[i]-0.99)*Activities[i]*(1-Activities[i]);
				else NodeDeltas[i] = (Activities[i]-0.01)*Activities[i]*(1-Activities[i]);
				for (int j = 0; j < PrevLayerCellAmount; j++)
				{
					Weights[i][j] -= LearnRate*NodeDeltas[i]*PrevLayer.GetActivity(j);
				}
				Biases[i] -= LearnRate*NodeDeltas[i];
			}
			PrevLayer.BackPropagate(this,LearnRate);
		}
		void BackPropagate(Layer UpperLayer, double LearnRate)
		{
			if (IsFirst) return;
			for (int i = 0; i < CellAmount; i++)
			{
				double ActivityDerivative = Activities[i]*(1-Activities[i]);
				double ErrorDerivative = 0;
				for (int j = 0; j < UpperLayer.CellAmount; j++)
				{
					//NodeDelta*Weight
					ErrorDerivative += UpperLayer.GetNodeDelta(j)*UpperLayer.GetWeight(j,i);
				}
				NodeDeltas[i] = ActivityDerivative*ErrorDerivative;
				for (int k = 0; k < PrevLayer.CellAmount; k++)
				{
					double SumDerivative = PrevLayer.GetActivity(k);
					double FinalDerivative = NodeDeltas[i]*SumDerivative;//ErrorDerivative*ActivityDerivative*SumDerivative;
					Weights[i][k] -= LearnRate*FinalDerivative;
				}
				Biases[i] -= LearnRate*NodeDeltas[i];
			}
		}
		void LoadInput(double Input[])
		{
			for (int i = 0; i < Math.min(Input.length,CellAmount); i++)
			{
				Activities[i] = Input[i];
			}
		}
		int GetAnswer()
		{
			int Max = 0;
			for (int i = 1; i < CellAmount; i++)
			{
				if (Activities[i] > Activities[Max]) Max = i;
			}
			return Max;
		}
		double GetActivity(int Count) {return Activities[Count];}
		double GetBias(int Count) {return Biases[Count];}
		double GetWeight(int This, int Prev) {return Weights[This][Prev];}
		int GetResult() {return PrevResult;}
		int GetCellAmount() {return CellAmount;}
		void SetActivity(int j, double Value) {Activities[j] = Value;}
		void SetWeight(int j, int k, double Value) {Weights[j][k] = Value;}
		double GetNodeDelta(int Count) {return NodeDeltas[Count];}
	}
	
	
	
	Layer LayersVector[] = new Layer[20];
	int LayersAmount = 0;
	Network(int InputAmount)
	{
		if (LayersAmount != 0) return;
		Layer FirstLayer = new Layer(InputAmount);
		LayersVector[0] = FirstLayer;
		LayersAmount = 1;
	}
	void AddLayer(int CellAmount)
	{
		Layer NewLayer = new Layer(CellAmount,LayersVector[LayersAmount-1]);
		LayersVector[LayersAmount] = NewLayer;
		LayersAmount++;
	}
	int Process(double Input[])
	{
		LayersVector[0].LoadInput(Input);
		for (int i = 1; i < LayersAmount; i++)
		{
			LayersVector[i].FeedForward();
		}
		for (int i = 0; i < LayersVector[LayersAmount-1].GetCellAmount(); i++)
			System.out.println((i+1) + " " + LayersVector[LayersAmount-1].GetActivity(i));
		System.out.println("");
		return LayersVector[LayersAmount-1].GetAnswer();
	}
	int ProcessNoLog(double Input[])
	{
		LayersVector[0].LoadInput(Input);
		for (int i = 1; i < LayersAmount; i++)
		{
			LayersVector[i].FeedForward();
		}
		return LayersVector[LayersAmount-1].GetAnswer();
	}
	void Display()
	{
		System.out.println(1);
		for (int j = 0; j < LayersVector[0].GetCellAmount(); j++)
		{
			System.out.print(" " + LayersVector[0].GetActivity(j));//System.out.print("Cell " + (j+1) + " " + LayersVector[0].GetActivity(j));
			//System.out.println("");
		}
		System.out.println("");
		for (int i = 1; i < LayersAmount; i++)
		{
			System.out.println((i+1));
			for (int j = 0; j < LayersVector[i].GetCellAmount(); j++)
			{
				System.out.print("Cell "  + (j+1) + " ");
				System.out.format("%.4f | %.4f |",LayersVector[i].GetActivity(j),LayersVector[i].GetBias(j));
				//for (int u = 0; u < LayersVector[i-1].GetCellAmount(); u++)
				//	System.out.format(" %.4f", LayersVector[i].GetWeight(j,u));
				System.out.println("");
			}
			System.out.println("");
		}
	}
	void DisplayResult()
	{
		for (int i = 0; i < LayersVector[LayersAmount-1].CellAmount; i++)
			System.out.format("%.4f ",LayersVector[LayersAmount-1].GetActivity(i)); System.out.println("");
	}
	void Train (double Input[], int Res)
	{
		ProcessNoLog(Input);
		LayersVector[LayersAmount-1].StartBackPropagate(Res, 0.15);
	}
}












